$(document).ready(function(){
    $("nav").sticky({
        topSpacing:0,
        zIndex:999,
    });

    AOS.init();

    $('.nav-item').click(function(e){
        e.preventDefault();

        let target = $(this).attr("href");

        $('html, body').get(0).scrollTo({
            top: $(target).offset().top - $('#main-nav').outerHeight(),
            behavior: 'smooth'
        });

        $(".navbar-collapse").collapse("hide");
    });

});